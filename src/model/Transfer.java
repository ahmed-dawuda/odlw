package model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "transfers")
public abstract class Transfer {

    @Id
    @Column(name = "id")
    private Number id;

    @JoinColumn(name = "worker")
    @ManyToOne
    private Worker worker;

    @JoinColumn(name = "zone")
    @ManyToOne
    private Zone zone;

    @Column(name = "date")
    private Date date;

    @Column(name = "time")
    private Time time;

    @JoinColumn(name = "originator")
    @ManyToOne
    private Transfer originator;

    @OneToMany(mappedBy = "transfer", fetch = FetchType.LAZY)
    private List<Good> goods = new ArrayList<>();

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Transfer getOriginator() {
        return originator;
    }

    public void setOriginator(Transfer originator) {
        this.originator = originator;
    }

    public List<Good> getGoods() {
        return goods;
    }

    public void setGoods(List<Good> goods) {
        this.goods = goods;
    }
}
