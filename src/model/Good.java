package model;

import javax.persistence.*;

@Entity
@Table(name = "goods")
public class Good {

    @Id
    @Column(name = "id")
    private Number id;

    @JoinColumn(name = "product")
    @ManyToOne
    private Product product;

    @Column(name = "quantity")
    private double quantity;

    @Column(name = "instock")
    private double instock;

    @JoinColumn(name = "transfer")
    @ManyToOne
    private Transfer transfer;

    @Column(name = "quantityRequested")
    private double quantityRequested;

    public double getQuantityRequested() {
        return quantityRequested;
    }

    public void setQuantityRequested(double quantityRequested) {
        this.quantityRequested = quantityRequested;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getInstock() {
        return instock;
    }

    public void setInstock(double instock) {
        this.instock = instock;
    }

    public Transfer getTransfer() {
        return transfer;
    }

    public void setTransfer(Transfer transfer) {
        this.transfer = transfer;
    }
}
