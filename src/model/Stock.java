package model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@Table(name = "stocks")
public class Stock {
    @Id
    @Column(name = "id")
    private Number id;

    @Column(name = "date")
    private Date date;

    @Column(name = "time")
    private Time time;

    @JoinColumn(name = "product")
    @ManyToOne
    private Product product;

    @Column(name = "quantity")
    private Double quantity;

    @Column(name = "instock")
    private Double instock;

    @JoinColumn(name = "worker")
    @ManyToOne
    private Worker worker;

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getInstock() {
        return instock;
    }

    public void setInstock(Double instock) {
        this.instock = instock;
    }

    public Worker getWorker() {
        return worker;
    }

    public void setWorker(Worker worker) {
        this.worker = worker;
    }
}
