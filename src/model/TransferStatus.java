package model;

import javax.persistence.*;

@Entity
@Table(name = "transferstatuses")
public class TransferStatus {

    @Id
    @Column(name = "id")
    private Number id;

    @ManyToOne
    @JoinColumn(name = "product")
    private Product product;

    @Column(name = "good")
    private double good;

    @Column(name = "bad")
    private double bad;

    @ManyToOne
    @JoinColumn(name = "goodsreceived")
    private GoodReceived goodsreceived;

    @Column(name = "total")
    private double total;

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getGood() {
        return good;
    }

    public void setGood(double good) {
        this.good = good;
    }

    public double getBad() {
        return bad;
    }

    public void setBad(double bad) {
        this.bad = bad;
    }

    public GoodReceived getGoodsreceived() {
        return goodsreceived;
    }

    public void setGoodsreceived(GoodReceived goodsreceived) {
        this.goodsreceived = goodsreceived;
    }
}
