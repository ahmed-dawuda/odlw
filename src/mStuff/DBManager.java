package mStuff;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DBManager {

    private static EntityManager em;

    private static EntityManagerFactory factory;

    public static boolean createDbDirectory(String dbLocation) {
        File dbFolder = new File(dbLocation);
        return dbFolder.exists() || dbFolder.mkdirs();
    }

    public static boolean init(String persistence_name){
        try {
            factory = Persistence.createEntityManagerFactory(persistence_name);
            em = factory.createEntityManager();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static void begin(){
        em.getTransaction().begin();
    }

    public static void commit(){
        em.getTransaction().commit();
    }

    public static <T> T save(Class<T> type, Object o){
        begin();
        em.persist(o);
        commit();

        return type.cast(o);
    }

    public static void delete(Object o){
        begin();
        em.remove(o);
        commit();
    }

    public static <T> T find(Class<T> type, Object id){
        return em.find(type, id);
    }

    public static List<Object[]> query(String query, Object... params){
        Query q = em.createQuery(query);
        for (int i = 0; i < params.length; i++) q.setParameter(i + 1, params[i]);
        final List resultList = q.getResultList();
        List<Object[]> result = new ArrayList<>();

        Class<Object[]> objectArrayClass = Object[].class;
        for (Object o : resultList) result.add(objectArrayClass.cast(o));

        return result;
    }

    public static <T> List<T> query(Class<T> type, String query, Object... params){
        Query q = em.createQuery(query);
        for (int i = 0; i < params.length; i++) q.setParameter(i + 1, params[i]);
        final List resultList = q.getResultList();
        List<T> result = new ArrayList<>();

        for (Object o : resultList) result.add(type.cast(o));

        return result;
    }

    public static <T> List<T> listAll(Class<T> type) {
        String className = type.getName().substring(type.getPackage().getName().length() + 1);
        Query q = em.createQuery("select e from " + className + " e");
        final List resultList = q.getResultList();
        List<T> result = new ArrayList<>();

        for (Object o : resultList) result.add(type.cast(o));

        return result;
    }

    public static <T> T queryForSingleResult(Class<T> type, String query, Object... params) {
        Query q = em.createQuery(query);
        for (int i = 0; i < params.length; i++) q.setParameter(i + 1, params[i]);
        return type.cast(q.getSingleResult());
    }

    public static long update(String query, Object... params){
        begin();
        Query q = em.createQuery(query);
        for (int i = 0; i < params.length; i++) q.setParameter(i + 1, params[i]);
        long i = q.executeUpdate();
        commit();

        return i;
    }

    public static boolean close(){
        try {
            em.close();
            factory.close();
        } catch (Exception e) {
            return false;
        }

        return true;
    }
}
