package mStuff;

import controller.popups.Notification;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class ViewUtility {

    public static String BASE_PACKAGE_NAME = "";
    public static String icon = "/view/icons/lion_icon.png";

    public static FXMLLoader loadFXML(String fxml) {
        return new FXMLLoader(fullPath(fxml));
    }

    public static FXMLLoader newWindow(String filename, String title, EventHandler<WindowEvent> onCloseRequest) throws IOException {
        FXMLLoader fxmlLoader = loadFXML(filename);
        Stage secondStage = new Stage();
        Parent root = fxmlLoader.load();
        secondStage.setScene(new Scene(root));
        secondStage.getIcons().add(new Image(icon));
        secondStage.setTitle(title);
        secondStage.setOnCloseRequest(onCloseRequest);
        secondStage.show();
        return fxmlLoader;
    }

    private static URL fullPath(String fxml) {
        return ViewUtility.class.getResource(BASE_PACKAGE_NAME + "/view/template/content/" + fxml + ".fxml");
    }

    public static FXMLLoader modal(String fxml, String title, boolean... resizeable) {
        Stage modalStage = new Stage();
        modalStage.getIcons().add(new Image(icon));
        modalStage.initModality(Modality.APPLICATION_MODAL);

        FXMLLoader loader = loadFXML(fxml);
        try {
            Parent root = loader.load();
            modalStage.setTitle(title);
            modalStage.setScene(new Scene(root));
            modalStage.setResizable(resizeable.length > 0);
            modalStage.sizeToScene();
//            modalStage.showAndWait();
            modalStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return loader;
    }

    public static void initTableColumn(TableColumn column, String property) {
        column.setCellValueFactory(new PropertyValueFactory<>(property));
    }

    public static void initColumns(List<TableColumn> columns, List<String> properties) {
        for (int i = 0; i < columns.size(); i++) {
            initTableColumn(columns.get(i), properties.get(i));
        }
    }

    public static void alert(String title, String message){
        FXMLLoader modal = ViewUtility.modal("popups/alerts/notification", "Desert Lion Group");
        Notification notificationController = modal.getController();
        notificationController.init(title, message);
    }

    public static void closeWindow(Node anchor){
        if (anchor == null) return;
        ((Stage) anchor.getScene().getWindow()).close();
    }
}
