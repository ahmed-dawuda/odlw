package controller;


import controller.popups.NewPayment;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.util.Callback;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Account;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Payment {

    /**
     * datepicker and text fields on the view
     */
    @FXML private DatePicker startDate;
    @FXML private DatePicker endDate;
    @FXML private TextField total;

    /**
     * table and table columns definition
     */
    @FXML private TableView<model.Payment> paymentTable;
    @FXML private TableColumn<model.Payment, Date> dateCol;
    @FXML private TableColumn<model.Payment, Time> timeCol;
    @FXML private TableColumn<model.Payment, Account> bankCol;
    @FXML private TableColumn<model.Payment, Double> amountCol;
    @FXML private TableColumn<model.Payment, Account> branchCol;
    @FXML private TableColumn<model.Payment, Account> accountCol;

    /**
     * properties
     */
//    private List<model.Payment> payments = new ArrayList<>();
    private utility.Payment paymentUtil = new utility.Payment();

    /**
     * initializing the view page
     */
    public void initialize(){
        /**
         * initializing tables
         */
        ViewUtility.initColumns(Arrays.asList(dateCol, timeCol, bankCol, amountCol, branchCol, accountCol), Arrays.asList("date", "time", "account", "amount", "account", "account"));

        /**
         * listeners
         */
        total.setAlignment(Pos.BASELINE_RIGHT);

        amountCol.setCellFactory(cell -> new TableCell<model.Payment, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        timeCol.setCellFactory(cell -> new TableCell<model.Payment, Time>() {
            @Override
            protected void updateItem(Time item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getTimeInstance().format(item));
            }
        });

        dateCol.setCellFactory(cell -> new TableCell<model.Payment, Date>() {
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        bankCol.setCellFactory(cell -> new TableCell<model.Payment, Account>() {
            @Override
            protected void updateItem(Account item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getBank());
            }
        });

        branchCol.setCellFactory(cell -> new TableCell<model.Payment, Account>() {
            @Override
            protected void updateItem(Account item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getBranch());
            }
        });

        accountCol.setCellFactory(cell -> new TableCell<model.Payment, Account>() {
            @Override
            protected void updateItem(Account item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getNumber());
            }
        });

        paymentTable.itemsProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                total.setText("");
            }
            else {
                double totalAmount = 0;
                for (model.Payment payment : newValue) {
                    totalAmount += payment.getAmount();
                }
                total.setText(GlobalValues.getCurrencyInstance().format(totalAmount));
            }
        });

        /**
         * fetching the payments
         */
//        payments = DBManager.listAll(model.Payment.class);

        paymentTable.setItems(FXCollections.observableArrayList(paymentUtil.all()));

        paymentTable.getItems().addListener((ListChangeListener<model.Payment>) c -> {
            ObservableList<? extends model.Payment> list = c.getList();
            double totalAmount = 0;
            for (model.Payment payment : list) {
                totalAmount += payment.getAmount();
            }
            total.setText(GlobalValues.getCurrencyInstance().format(totalAmount));
        });
    }

    public void makePayment(){
        FXMLLoader modal = ViewUtility.modal("popups/makepayment", "Make new Payment", true);
        NewPayment newPaymentController = modal.getController();
        newPaymentController.init(paymentTable.getItems());
    }

    public void filter(){
        LocalDate start = startDate.getValue() == null ? LocalDate.of(2015, 01, 01) : startDate.getValue();
        LocalDate end = endDate.getValue() == null ? LocalDate.now().plusDays(1) : endDate.getValue().plusDays(1);
        List<model.Payment> payments = paymentUtil.filterByDates(Date.valueOf(start), Date.valueOf(end));
        paymentTable.getItems().clear();
        paymentTable.getItems().addAll(payments);
    }
}
