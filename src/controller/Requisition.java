package controller;

import controller.popups.IssueGood;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Product;
import model.Transfer;
import utility.*;
import utility.Inventory;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Requisition {

    @FXML private TableView<Transfer> requestTable;
    @FXML private TableColumn<Transfer, Date> dateCol;
    @FXML private TableColumn<Transfer, Zone> zoneCol;
    @FXML private TableColumn<Transfer, Number> statusCol;

    @FXML private TableView<Good> goodsTable;
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Double> instockCol;

    private utility.Transfer transferUtil = new utility.Transfer();
    private utility.Inventory inventoryUtil = new Inventory();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, zoneCol, statusCol), Arrays.asList("date", "zone", "id"));
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, instockCol), Arrays.asList("product", "quantity", "instock"));

        dateCol.setCellFactory(c -> new TableCell<Transfer, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        zoneCol.setCellFactory(c -> new TableCell<Transfer, Zone>(){
            @Override
            protected void updateItem(Zone item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        statusCol.setCellFactory(c -> new TableCell<Transfer, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : transferUtil.getIssueCommandStatus(item));
            }
        });

        productCol.setCellFactory(c -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        instockCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });
        goodsTable.setItems(FXCollections.observableArrayList());
        requestTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            goodsTable.getItems().clear();
            goodsTable.getItems().addAll(newValue.getGoods());
        });
        requestTable.setItems(FXCollections.observableArrayList(transferUtil.issueCommands()));
        requestTable.getSelectionModel().selectFirst();
    }

    public void filter(ActionEvent actionEvent) {

    }

    public void issue(){
        Transfer selectedItem = requestTable.getSelectionModel().getSelectedItem();
        String issueCommandStatus = transferUtil.getIssueCommandStatus(selectedItem.getId());
        if (issueCommandStatus.equalsIgnoreCase("Issued")) {
            ViewUtility.alert("Sorry Request Already Issued", "You have already issued this request to the requesting zone.");
        } else {
            FXMLLoader modal = ViewUtility.modal("popups/issueGoods", "Issue Good To Zonal Officer");
            IssueGood controller = modal.getController();
            controller.init((IssueCommand) selectedItem);
        }
    }
}
