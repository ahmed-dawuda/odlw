package controller.popups;


import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import model.Customer;

public class CustDetail {
    @FXML private TextField name;
    @FXML private TextField phone;
    @FXML private TextField address;

    public void initialize(){

    }

    public void init(Customer customer){
        name.setText(customer.getFullName());
        phone.setText(customer.getPhone());
        address.setText(customer.getAddress());
        name.setFocusTraversable(false);
        phone.setFocusTraversable(false);
        address.setFocusTraversable(false);
    }
}
