package controller.popups;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import mStuff.ViewUtility;

public class Logout {
    @FXML private Button yes;

    public void initialize(){

    }


    public void logoutAction(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText().toLowerCase();
        if (text.trim().equalsIgnoreCase("yes")) {
            ViewUtility.closeWindow(yes);
        } else {
            ViewUtility.closeWindow(yes);
        }
    }
}
