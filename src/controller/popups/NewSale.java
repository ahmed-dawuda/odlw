package controller.popups;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
//import model.*;
//import model.Inventory;
import model.*;
import utility.Inventory;
//import utility.*;
//import utility.Inventory;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NewSale {

    /**
     * form fields
     */
    @FXML private TextField customername;
    @FXML private ChoiceBox<Product> productCB;
    @FXML private TextField quantityTF;
    @FXML private TextField amountTF;
    @FXML private TextField priceTF;
    @FXML private TextField discountTF;
    @FXML private TextField totalTF;
    @FXML private TextField payableTF;
    @FXML private ChoiceBox<Location> wlocation;

    /**
     * table and columns
     */
    @FXML private TableView<Sale> saleItemsTable;
    @FXML private TableColumn<Sale, Product> productCol;
    @FXML private TableColumn<Sale, Double> quantityCol;
    @FXML private TableColumn<Sale, Double> amountCol;
    @FXML private TableColumn<Sale, Double> priceCol;
    @FXML private TableColumn<Sale, Number> controlCol;

    /**
     * properties
     */
    private List<Invoice> invoices = new ArrayList<>();
    private utility.Inventory inventoryUtil = new Inventory();
    private utility.Invoice invoiceUtil = new utility.Invoice();
    private utility.Product productUtil = new utility.Product();
//    private Inventory

    private List<Sale> saleItems = new ArrayList<>();

    private Invoice newInvoice = null;

    private Location saleLocation = null;

    public void initialize(){
        discountTF.setText("0");
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, amountCol, priceCol, controlCol), Arrays.asList("product", "quantity", "amount", "price", "id"));

        wlocation.setConverter(new StringConverter<Location>() {
            @Override
            public String toString(Location object) {
                return object.getName();
            }

            @Override
            public Location fromString(String string) {
                return null;
            }
        });

        wlocation.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//           if (saleItemsTable.getItems().size() > 0) {
//               ViewUtility.alert("Sorry", "You can only make sales from one location. Clear the items table before changing location");
//               wlocation.getSelectionModel().select(saleLocation);
//               return;
//           } else {
//               saleLocation = newValue;
//           }
            if (saleItemsTable.getItems().size() > 0) {
                saleItemsTable.getItems().clear();
                ViewUtility.alert("Oops. Sale items table has been cleared", "You cant make a sale from multiple location at a time");
            }
            saleLocation = newValue;
        });

        wlocation.setItems(FXCollections.observableArrayList(GlobalValues.getWorker().getLocations()));
        wlocation.getSelectionModel().select(GlobalValues.getWorker().getPrimaryLocation());

        controlCol.setCellFactory(cell ->  new TableCell<Sale, Number>() {
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setOnAction(event -> {
                    int index = getTableRow().getIndex();
//                    System.out.println(saleItemsTable.getItems().get(index).getProduct().getName());
                    saleItemsTable.getItems().remove(index);
                });
                link.setTextFill(Color.valueOf("#e65100"));
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        productCol.setCellFactory(cell -> new TableCell<Sale, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });


        quantityCol.setCellFactory(cell -> new TableCell<Sale, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        amountCol.setCellFactory(cell -> new TableCell<Sale, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        priceCol.setCellFactory(cell -> new TableCell<Sale, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });



        productCB.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getName();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });

        quantityTF.textProperty().addListener((observable, oldValue, newValue) -> {
            double qty, price;

            if (newValue.isEmpty()) return;

            try{
                qty = Double.parseDouble(newValue);
                model.Inventory inventory = inventoryUtil.getInventoryByProductAndLocation(productCB.getSelectionModel().getSelectedItem(), (model.Warehouse) saleLocation);
                if (inventory.getQuantity() >= qty) {
                    price = productCB.getSelectionModel().getSelectedItem().getPrice();
                    amountTF.setText(GlobalValues.getDefaultDecimalFormat().format(qty * price));
                } else {
                    quantityTF.setText(oldValue);
                    ViewUtility.alert("Insufficient quantity", "You have insufficient quantity of " + inventory.getProduct().getName() +" in your inventory");
                }
            }catch (Exception ex){
                quantityTF.setText(oldValue);
            }
        });

        productCB.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            priceTF.setText(GlobalValues.getDefaultDecimalFormat().format(productCB.getSelectionModel().getSelectedItem().getPrice()));
            if(quantityTF.getText().trim().isEmpty()) {
                quantityTF.setText("0");
            }
        });

        priceTF.textProperty().addListener((observable, oldValue, newValue) -> {
            String string = quantityTF.getText().trim().isEmpty() ? "0" : quantityTF.getText().trim();
           double quantity = Double.parseDouble(string);
           amountTF.setText(GlobalValues.getDefaultDecimalFormat().format(quantity * Double.parseDouble(newValue)));
        });

        productCB.setItems(FXCollections.observableArrayList(productUtil.all()));
        productCB.getSelectionModel().selectFirst();
        saleItemsTable.setItems(FXCollections.observableArrayList());

        saleItemsTable.getItems().addListener((ListChangeListener<Sale>) c -> {
            ObservableList<? extends Sale> list = c.getList();
            double total = 0;
            for (Sale sale : list) {
                total += sale.getAmount();
            }

            totalTF.setText(GlobalValues.getDefaultDecimalFormat().format(total));
            double discount = Double.parseDouble(discountTF.getText());
            payableTF.setText(GlobalValues.getDefaultDecimalFormat().format(total - discount));
        });

        discountTF.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.trim().isEmpty() ? "0" : newValue;
            double discount = Double.parseDouble(newValue);
            double total = Double.parseDouble(totalTF.getText());
            payableTF.setText(GlobalValues.getDefaultDecimalFormat().format(total - discount));
        });

    }


    public void sell(){
        if (customername.getText().trim().isEmpty()){
            ViewUtility.alert("Customer name is required", "You have'nt provided the name of the customer, it is required to make a sale");
        } else if(saleItemsTable.getItems().size() < 1) {
            ViewUtility.alert("Sales list is empty", "You have to add one or more items to the sales list");
        } else {
            List<Sale> sales = saleItemsTable.getItems();
            Invoice invoice = invoiceUtil.create(this.saleLocation, customername.getText(), Double.parseDouble(discountTF.getText()), Double.parseDouble(payableTF.getText()), Double.parseDouble(totalTF.getText()), "cash");

            for (Sale sale : sales) {

                sale.setInvoice(invoice);
                DBManager.save(Sale.class, sale);

                model.Inventory inventory = inventoryUtil.getInventoryByProductAndLocation(sale.getProduct(), (Warehouse) this.saleLocation);
                DBManager.begin();
                inventory.setQuantity(inventory.getQuantity() - sale.getQuantity());
                DBManager.commit();
            }
            DBManager.begin();
            invoice.setSales(sales);
            DBManager.commit();

            saleItemsTable.getItems().clear();
            discountTF.setText("0");
            totalTF.clear();
            payableTF.clear();
            customername.clear();
            invoices.add(invoice);
            ViewUtility.alert("Success", "Sales made successfully");
            ViewUtility.closeWindow(saleItemsTable);
        }

    }

    public void addItem(){
        List<Sale> sales = saleItemsTable.getItems();

        model.Inventory inventory = inventoryUtil.getInventoryByProductAndLocation(productCB.getSelectionModel().getSelectedItem(), (Warehouse) this.saleLocation);
        quantityTF.setText(quantityTF.getText().isEmpty() ? "0" : quantityTF.getText());
         if (inventory != null){
             if (inventory.getQuantity() < Double.parseDouble(quantityTF.getText())){
                 ViewUtility.alert("Insufficient quantity in stock", "You have insufficient amount of " +
                         productCB.getSelectionModel().getSelectedItem().getName() + " in inventory. Available quantity: " + inventory.getQuantity());
                 return;
             }
         } else {
             ViewUtility.alert("Product not available", "You do not have this product in your inventory");
             return;
         }

        for (Sale sale : sales) {
            if (sale.getProduct().getName().equalsIgnoreCase(productCB.getSelectionModel().getSelectedItem().getName())){
                ViewUtility.alert("Product already exist", "You've already added this item. Please remove it before adding again");
                saleItemsTable.getSelectionModel().select(sale);
                return;
            }
        }
        if (!quantityTF.getText().trim().equalsIgnoreCase("0")){
            Sale sale = new Sale();
            sale.setId(GlobalValues.generateId());
            sale.setAmount(Double.parseDouble(amountTF.getText()));
            sale.setPrice(productCB.getSelectionModel().getSelectedItem().getPrice());
            sale.setProduct(productCB.getSelectionModel().getSelectedItem());
            sale.setQuantity(Double.parseDouble(quantityTF.getText()));
            saleItemsTable.getItems().add(sale);
        } else {
            ViewUtility.alert("Quantity can not be zero", "You must provide the quantity of product to sell");
        }
    }


    public void init(List<Invoice> invoices){
        this.invoices = invoices;
    }
}
