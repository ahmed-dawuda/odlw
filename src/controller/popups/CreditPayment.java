package controller.popups;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextField;
import mStuff.DBManager;
import mStuff.ViewUtility;
import model.CreditSaleInvoice;
import utility.CashBook;
import utility.Invoice;

import java.util.ArrayList;
import java.util.List;


public class CreditPayment {

    public TextField name;
    public TextField amount;

    private CreditSaleInvoice creditSaleInvoice = null;
    private List<CreditSaleInvoice> invoices = new ArrayList<>();

    private CashBook cashBookUtil = new CashBook();

    public void initialize(){
        amount.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try {
                double a = Double.parseDouble(newValue);
            } catch (Exception e){
                amount.setText(oldValue);
            }
        });
    }

    public void save(){
         amount.setText(amount.getText().isEmpty() ? "0" : amount.getText());
        if (amount.getText().equalsIgnoreCase("0")) {
            ViewUtility.alert("Amount can not be zero", "Enter the amount paid");
        } else if (creditSaleInvoice != null) {
            this.invoices.remove(creditSaleInvoice);
            cashBookUtil.credit(Double.parseDouble(amount.getText()), "credit sale payment");
            DBManager.begin();
            creditSaleInvoice.setAmountPaid(creditSaleInvoice.getAmountPaid() + Double.parseDouble(amount.getText()));
            creditSaleInvoice.setBalance(creditSaleInvoice.getPayable() - creditSaleInvoice.getAmountPaid());
            DBManager.commit();
            this.invoices.add(creditSaleInvoice);
            ViewUtility.closeWindow(name);
        }
    }

    public void init(CreditSaleInvoice invoice, List<CreditSaleInvoice> invoices){
        this.creditSaleInvoice = invoice;
        this.invoices = invoices;
        if (invoice != null) {
            this.name.setText(invoice.getDebtor().getFullName());
        }
    }
}
