package controller.popups;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Location;
import model.Product;
import utility.Transfer;
import utility.Inventory;
import wrappers.GoodWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IssueGood {
    public TextField product;
    public TextField quantityApproved;
    public ChoiceBox<Location> warehouse;
    public TextField quantityInstock;

    public TableView<GoodWrapper> goodsTable;
    public TableColumn<GoodWrapper, Product> productCol;
    public TableColumn<GoodWrapper, Double> quantityCol;
    public TableColumn<GoodWrapper, Warehouse> warehouseCol;
    public TableColumn<GoodWrapper, Double> instockCol;
    public TableColumn<GoodWrapper, Number> editCol;
    public TableColumn<GoodWrapper, Number> removeCol;

    private utility.Location locationUtil = new utility.Location();
    private IssueCommand issueCommand = null;
    private Inventory inventoryUtil = new Inventory();
    private utility.Product productUtil = new utility.Product();
    private Transfer transferUtil = new Transfer();

    private GoodWrapper selectedGoodWrapper = null;

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, warehouseCol, instockCol, editCol, removeCol),
                Arrays.asList("product", "quantityApproved", "supplyFrom", "instock", "id", "id"));
        product.setAlignment(Pos.BASELINE_LEFT);

        warehouse.setConverter(new StringConverter<Location>() {
            @Override
            public String toString(Location object) {
                return object.getName();
            }

            @Override
            public Location fromString(String string) {
                return null;
            }
        });
        warehouse.setItems(FXCollections.observableArrayList(GlobalValues.getWorker().getLocations()));

        productCol.setCellFactory(c -> new TableCell<GoodWrapper, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<GoodWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        warehouseCol.setCellFactory(c -> new TableCell<GoodWrapper, Warehouse>(){
            @Override
            protected void updateItem(Warehouse item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        instockCol.setCellFactory(c -> new TableCell<GoodWrapper, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        product.textProperty().addListener((observable, oldValue, newValue) -> {
            Location selectedItem = warehouse.getSelectionModel().getSelectedItem();
            model.Inventory item = inventoryUtil.getInventoryByProductAndLocation(productUtil.productByName(newValue), (Warehouse) selectedItem);
            quantityInstock.setText(GlobalValues.getDefaultDecimalFormat().format(item.getQuantity()));
        });

        removeCol.setCellFactory(c -> new TableCell<GoodWrapper, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                link.setOnAction(e -> {
                    goodsTable.getItems().remove(getTableRow().getIndex());
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        warehouse.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (!product.getText().trim().isEmpty()) {
                Product product = productUtil.productByName(this.product.getText());
                model.Inventory inventoryByProductAndWarehouse = locationUtil.getInventoryByProductAndWarehouse(product, ((Warehouse) newValue));
                if (inventoryByProductAndWarehouse != null) {
                    quantityInstock.setText(GlobalValues.getDefaultDecimalFormat().format(inventoryByProductAndWarehouse.getQuantity()));
                } else {
                    ViewUtility.alert("Sorry, insufficient stock at " + newValue.getName(), "The warehouse location you selected does not have this product");
                    warehouse.getSelectionModel().select(GlobalValues.getWarehouse());
                }
            }
        });

        editCol.setCellFactory(c -> new TableCell<GoodWrapper, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    int index = getTableRow().getIndex();
                    GoodWrapper good = goodsTable.getItems().get(index);
                    warehouse.getSelectionModel().select(good.getSupplyFrom());
                    product.setText(good.getProduct().getName());
                    quantityApproved.setText(GlobalValues.getDefaultDecimalFormat().format(good.getQuantityApproved()));
                    selectedGoodWrapper = good;
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });
    }

    public void add() {
        if(this.warehouse.getSelectionModel().getSelectedItem() == null ) {
            return;
        }

        GoodWrapper goodWrapper = new GoodWrapper();
        goodWrapper.setId(GlobalValues.generateId());
        goodWrapper.setProduct(selectedGoodWrapper.getProduct());
        goodWrapper.setQuantityApproved(this.selectedGoodWrapper.getQuantityApproved());
        goodWrapper.setSupplyFrom((Warehouse) warehouse.getSelectionModel().getSelectedItem());
        goodWrapper.setInstock(Double.parseDouble(quantityInstock.getText()));

        goodsTable.getItems().remove(this.selectedGoodWrapper);
        goodsTable.getItems().add(goodWrapper);
    }

    public void approve(ActionEvent event) {
        List<GoodWrapper> goodWrappers = goodsTable.getItems();

        Issue issue = transferUtil.createIssue(this.issueCommand);

        List<Good> goods = new ArrayList<>();

        for (GoodWrapper goodWrapper : goodWrappers) {
            Good good = goodWrapper.toGood(issue);
            updateInventory(goodWrapper);
            DBManager.save(Good.class, good);
            goods.add(good);
        }

        DBManager.begin();
        issue.setGoods(goods);
        DBManager.commit();
        ViewUtility.closeWindow(goodsTable);
        ViewUtility.alert("Success", "You have successfully issued the good to the zone");
    }

    public void updateInventory(GoodWrapper good){
        model.Inventory inventory = inventoryUtil.getInventoryByProductAndLocation(good.getProduct(), good.getSupplyFrom());
        DBManager.begin();
        inventory.setPrevQty(inventory.getQuantity());
        inventory.setQuantity(inventory.getQuantity() - good.getQuantityApproved());
        DBManager.commit();
    }

    public void init(IssueCommand issueCommand){
        this.issueCommand = issueCommand;
        if (this.issueCommand != null) {
            goodsTable.getItems().clear();
            List<Good> goods = this.issueCommand.getGoods();
            List<GoodWrapper> goodWrappers = new ArrayList<>();

            for (Good good : goods) {
                goodWrappers.add(new GoodWrapper(good));
            }

            goodsTable.getItems().addAll(goodWrappers);
        }
    }

}
