package controller.popups;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import mStuff.ViewUtility;
import model.Good;
import model.Product;

import java.text.DecimalFormat;
import java.util.Arrays;

public class GoodReceivedNote {

    @FXML private TextField qtyGood;
    @FXML private TextField qtyBad;
    @FXML private TextField totalQty;
    @FXML private ChoiceBox<Product> productCB;

    @FXML private TableView<Good> goodsTable;
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Number> controlsCol;

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, controlsCol), Arrays.asList("product", "quantity", "id"));

        productCB.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getName();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });

        productCol.setCellFactory(cell -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(cell -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : new DecimalFormat("#0.00").format(item));
            }
        });

        productCB.setItems(FXCollections.observableArrayList());

        controlsCol.setCellFactory(cell -> new TableCell<Good, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Comment");
                link.setOnAction(event -> {
                    productCB.getItems().clear();
                });
            }
        });
    }
}
