package controller.popups;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import mStuff.DBManager;
import mStuff.ViewUtility;
import model.Customer;
import utility.Customers;

import java.util.ArrayList;
import java.util.List;

public class RegisterCustomer {
    public TextField name;
    public TextField phone;
    public TextField address;

    private List<Customer> customers = new ArrayList<>();
    private Customer customer = null;

    private Customers customerUtil = new Customers();

    public void initialize(){
        phone.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try {
                double p = Double.parseDouble(newValue);
            } catch (Exception e){
                phone.setText(oldValue);
            }
        });
    }

    public void register(ActionEvent event) {
        if (name.getText().isEmpty()){
            ViewUtility.alert("Name is required", "You must enter the name of the Company or individual you wish to register");
            return;
        } else if (phone.getText().isEmpty()) {
            ViewUtility.alert("Phone is required", "You must enter the contact phone of the Company or individual you wish to register");
            return;
        } else if (address.getText().isEmpty()) {
            ViewUtility.alert("Address is required", "You must enter the Address of the Company or individual you wish to register");
            return;
        } else if (this.customer == null) {
            Customer customer = customerUtil.createCustomer(name.getText().trim(), phone.getText().trim(), address.getText().trim());
            customers.add(customer);
            ViewUtility.closeWindow(name);
            ViewUtility.alert("Success", "Customer registered successfully");
        } else {
            this.customers.remove(this.customer);
            DBManager.begin();
            this.customer.setPhone(phone.getText().trim());
            this.customer.setFullName(name.getText().trim());
            this.customer.setAddress(address.getText().trim());
            DBManager.commit();
            this.customers.add(this.customer);
            ViewUtility.closeWindow(name);
            ViewUtility.alert("Success", "Customer update successfully");
        }
    }

    public void init(List<Customer> customerList, Customer... custs){
        this.customers = customerList;
        if (custs.length > 0) {
            this.customer = custs[0];
            name.setText(this.customer.getFullName());
            address.setText(this.customer.getAddress());
            phone.setText(this.customer.getPhone());
        }
    }
}
