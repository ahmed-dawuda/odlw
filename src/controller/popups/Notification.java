package controller.popups;


import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Notification {

    @FXML private Label title;
    @FXML private Label message;


    public void initialize(){

    }

    /**
     * this method is called by whatever called this notification
     */
    public void init(String titleText, String messageText){
        title.setText(titleText);
        message.setText(messageText);
    }
}
