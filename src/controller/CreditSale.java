package controller;


import controller.popups.CreditPayment;
import controller.popups.CustDetail;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Customer;
import model.Location;
import utility.*;
import utility.Invoice;

import java.sql.Date;
import java.sql.Time;
import java.util.Arrays;

public class CreditSale {


    @FXML private DatePicker startDate;
    @FXML private DatePicker endDate;
    @FXML private TableView<CreditSaleInvoice> salesTable;
    @FXML private TableColumn<CreditSaleInvoice, Date> dateCol;
    @FXML private TableColumn<CreditSaleInvoice, model.Customer> customerCol;
    @FXML private TableColumn<CreditSaleInvoice, Double> amountCol;
    @FXML private TableColumn<CreditSaleInvoice, Double> discountCol;
    @FXML private TableColumn<CreditSaleInvoice, Double> netCol;
    @FXML private TableColumn<CreditSaleInvoice, Location> locationCol;
    @FXML private TableColumn<CreditSaleInvoice, Number> viewCol;
    @FXML private TableColumn<CreditSaleInvoice, Double> balanceCol;
    @FXML private TableColumn<CreditSaleInvoice, Double> paidCol;
    @FXML private TableColumn<CreditSaleInvoice, Number> makePaymentCol;


    @FXML private TextField totalSales;
    @FXML private TextField totalDiscount;
    @FXML private TextField netSales;

    private utility.Invoice invoiceUtil = new Invoice();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, customerCol, amountCol, discountCol, netCol, locationCol, viewCol, balanceCol, paidCol, makePaymentCol),
                Arrays.asList("date", "debtor", "total", "discount", "payable", "location", "id", "balance", "amountPaid", "id"));

        dateCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        customerCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Customer>(){
            @Override
            protected void updateItem(Customer item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                }else {
                    Hyperlink link = new Hyperlink(item.getFullName());
                    link.setTextFill(Color.DARKGREEN);
                    link.setOnAction(e -> {
                        FXMLLoader modal = ViewUtility.modal("popups/custDetail", "Customer details");
                        CustDetail controller = modal.getController();
                        controller.init(item);
                    });
                    setAlignment(Pos.BASELINE_CENTER);
                    setGraphic(link);
                }
            }
        });

        amountCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        discountCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        netCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        locationCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        viewCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Detail");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {

                });
                setAlignment(Pos.BASELINE_CENTER);
                setGraphic(item == null || empty ? null : link);
            }
        });

        balanceCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        paidCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        makePaymentCol.setCellFactory(c -> new TableCell<CreditSaleInvoice, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Pay");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/creditPayment", "Make Credit Payment");
                    CreditPayment controller = modal.getController();
                    controller.init(salesTable.getItems().get(getTableRow().getIndex()), salesTable.getItems());
                });
                setAlignment(Pos.BASELINE_CENTER);
                setGraphic(item == null || empty ? null : link);
            }
        });

        salesTable.setItems(FXCollections.observableArrayList(invoiceUtil.creditSaleInvoices()));
    }

    public void filter(ActionEvent event) {

    }

    public void newSale(ActionEvent event) {
        FXMLLoader modal = ViewUtility.modal("popups/creditSale", "Make a new Credit Sale");
        controller.popups.CreditSale controller = modal.getController();
        controller.init(salesTable.getItems());
    }
}
