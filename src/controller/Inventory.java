package controller;


import controller.popups.NewStock;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Location;
import model.Product;

import java.text.DecimalFormat;
import java.util.Arrays;

public class Inventory {

    @FXML private TableView<model.Inventory> inventoryTable;
    @FXML private TableColumn<model.Inventory, Product> productCol;
    @FXML private TableColumn<model.Inventory, Double> quantityCol;
    @FXML private TableColumn<model.Inventory, Product> priceCol;
    @FXML private TableColumn<model.Inventory, Double> amountCol;

    @FXML private TableView<Location> locationsTable;
    @FXML private TableColumn<Location, String> nameCol;

    @FXML private TextField totalAmount;

    private utility.Inventory inventoryUtil = new utility.Inventory();

    public void initialize(){
        ViewUtility.initTableColumn(nameCol, "name");

        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, priceCol), Arrays.asList("product", "quantity", "product"));


        amountCol.setCellValueFactory(cellData -> {
            double amount = cellData.getValue().getQuantity() * cellData.getValue().getProduct().getPrice();
            return new SimpleObjectProperty<>(amount);
        });

        productCol.setCellFactory(cell -> new TableCell<model.Inventory, Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(cell -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item ==  null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
                if (item != null && !empty) {
                    int index = getTableRow().getIndex();
                    model.Inventory inventory = inventoryTable.getItems().get(index);
                    setStyle(item < inventory.getMinStock() ? "-fx-font-weight: bold; -fx-text-fill: #e65100" : "-fx-font-weight: normal");
                }
            }
        });

        priceCol.setCellFactory(cell -> new TableCell<model.Inventory, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item.getPrice()));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        amountCol.setCellFactory(cell -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        inventoryTable.setItems(FXCollections.observableArrayList(inventoryUtil.all()));

//        double total = 0;
//
//        for (model.Inventory inventory : inventoryTable.getItems()) {
//            total += (inventory.getQuantity() * inventory.getProduct().getPrice());
//        }
//        totalAmount.setText(GlobalValues.getCurrencyInstance().format(total));


        locationsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            inventoryTable.getItems().clear();
            inventoryTable.getItems().addAll(newValue.getInventory());
        });

        locationsTable.setItems(FXCollections.observableArrayList(GlobalValues.getWorker().getLocations()));
        locationsTable.getSelectionModel().selectFirst();
    }

    public void newStock(){
        Location selectedItem = locationsTable.getSelectionModel().getSelectedItem();
        if (selectedItem == null) {
            ViewUtility.alert("Location is required", "Please select a warehouse location you want to update the inventory");
            return;
        }
        FXMLLoader modal = ViewUtility.modal("popups/stock", "Add to Stock", true);
        NewStock controller = modal.getController();
        controller.init(selectedItem);
    }
}
