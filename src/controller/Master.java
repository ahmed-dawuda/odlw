package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import mStuff.ViewUtility;

import java.io.IOException;

public class Master {


    public VBox content;
    public Button requisitions;
    public Button sales;
    public Button payments;
    public Button inventory;
    public Label header;

    public void initialize() throws IOException {
        loadContent("inventory");
    }

    public void changeContent(ActionEvent event) throws IOException {
        String text = ((Button) event.getSource()).getId().toLowerCase();
        if (text.equalsIgnoreCase("logout")) {
            FXMLLoader modal = ViewUtility.modal("popups/alerts/logout", "Confirm logout");
        } else {
           loadContent(text);
        }
    }

    public void loadContent(String text) throws IOException {
        header.setText("Desert Lion Group > " + text.toUpperCase());
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/" + text + ".fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }
}
