package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Product;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Inventory {

    public List<model.Inventory> all(){
        return DBManager.query(model.Inventory.class, "SELECT i FROM Inventory i WHERE i.location = ?1", GlobalValues.getWarehouse());
    }

    /**
     *
     * @return products that are in the inventory. similar to select product from inventories
     */
    public List<model.Product> products(){
        List<model.Inventory> inventories = all();
        List<Product> products = new ArrayList<>();
        for (model.Inventory inventory : inventories) {
            products.add(inventory.getProduct());
        }
        return products;
    }

    public model.Inventory getInventoryByProduct(Product product){
        String query = "select i from Inventory i where i.product = ?1 and i.location = ?2";
        List<model.Inventory> inventories = DBManager.query(model.Inventory.class, query, product, GlobalValues.getWarehouse());
        model.Inventory inventory = null;
        if (inventories.size() > 0) {
            inventory = inventories.get(0);
        }
        return inventory;
    }

    public model.Inventory updateInventoryItem(model.Inventory inventory, double quantity){
        DBManager.begin();
        inventory.setPrevQty(inventory.getQuantity());
        inventory.setQuantity(inventory.getQuantity() + quantity);
        inventory.setLastUpdated(new Date(System.currentTimeMillis()));
        DBManager.commit();
        return inventory;
    }

    public model.Inventory getInventoryByProductAndLocation(Product product, Warehouse warehouse) {
        model.Inventory inventory = null;
        List<model.Inventory> inventories = DBManager.query(model.Inventory.class, "Select i from Inventory i where i.location = ?1 and i.product = ?2", warehouse, product);
        if (inventories.size() > 0) {
            inventory = inventories.get(0);
        }
        return inventory;
    }
}
