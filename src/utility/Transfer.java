package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Requisition;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class Transfer {

    public model.Transfer create(){
        Requisition requisition = new Requisition();
        requisition.setId(GlobalValues.generateId());
        requisition.setDate(new Date(System.currentTimeMillis()));
        requisition.setTime(new Time(System.currentTimeMillis()));
        DBManager.save(Requisition.class, requisition);
        return requisition;
    }

    public List<Requisition> allRequisitions(){
        return DBManager.query(Requisition.class, "Select r from Requisition r order by r.date");
    }

    public List<Good> getGoodsByRequisition(Requisition requisition){
        return DBManager.query(Good.class, "select g from Good g where g.transfer = ?1", requisition);
    }

    public String getStatus(model.Transfer originator){
        String status = "Pending";

        long integer = DBManager.queryForSingleResult(Long.class, "select count(g.id) from GoodReceived g where g.originator = ?1", originator);
        if (integer > 0) {
            return "Completed";
        }
        long integer1 = DBManager.queryForSingleResult(Long.class, "select count(g.id) from IssueCommand g where g.originator = ?1", originator);
        if (integer1 > 0) {
            return "Approved";
        }
        long integer2 = DBManager.queryForSingleResult(Long.class, "select count(g.id) from Issue g where g.originator = ?1", originator);
        if (integer2 > 0) {
            return "Issued";
        }

        return status;
    }

    public String getIssueCommandStatus(Number id){
        String status = "Approved";

        model.Transfer originator = DBManager.find(IssueCommand.class, id).getOriginator();

        long integer = DBManager.queryForSingleResult(Long.class, "select count(g.id) from GoodReceived g where g.originator = ?1", originator);
        if (integer > 0) {
            return "Completed";
        }

        long integer2 = DBManager.queryForSingleResult(Long.class, "select count(g.id) from Issue g where g.originator = ?1", originator);
        if (integer2 > 0) {
            return "Issued";
        }
        return status;
    }

    public String type(Number id){
        model.Transfer transfer = DBManager.find(model.Transfer.class, id);
        return getStatus(transfer);
    }

    public List<IssueCommand> issueCommands(){
        return DBManager.query(IssueCommand.class, "select i from IssueCommand i order by i.date desc");
    }

    public Good createGood(model.Transfer transfer, model.Product product, double quantity, double instock, double quantityRequested){
        Good good = new Good();
        good.setId(GlobalValues.generateId());
        good.setProduct(product);
        good.setInstock(instock);
        good.setQuantity(quantity);
        good.setQuantityRequested(quantityRequested);
        if (transfer != null) {
            good.setTransfer(transfer);
            DBManager.save(Good.class, good);
        }
        return good;
    }

    public Issue createIssue(IssueCommand issueCommand) {
        Issue issue = new Issue();
        issue.setId(GlobalValues.generateId());
        issue.setOriginator(issueCommand.getOriginator());
        issue.setTime(new Time(System.currentTimeMillis()));
        issue.setDate(new Date(System.currentTimeMillis()));
        issue.setWorker(issueCommand.getWorker());
        issue.setZone(issueCommand.getZone());
        DBManager.save(Issue.class, issue);
        return issue;
    }
}
