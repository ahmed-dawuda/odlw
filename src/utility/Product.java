package utility;


import mStuff.DBManager;

import java.util.List;

public class Product {

    public List<model.Product> all(){
        return DBManager.listAll(model.Product.class);
    }

    public model.Product productByName(String name) {
        return DBManager.query(model.Product.class, "Select p from Product p where p.name = ?1", name).get(0);
    }
}
