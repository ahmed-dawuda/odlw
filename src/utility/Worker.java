package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.Warehouse;

import java.util.List;

public class Worker {
    public boolean login(String email, String password){
        boolean result = false;
        if (email.isEmpty() || password.isEmpty()){
            return result;
        }

        List<model.Worker> worker = DBManager.query(model.Worker.class, "SELECT w FROM Worker w where w.email = ?1", email);
        if (worker.size() > 0) {
            if (worker.get(0).getPassword().equalsIgnoreCase(password) && worker.get(0).getLevel() == 1) {
               GlobalValues.setWorker(worker.get(0));
               GlobalValues.setWarehouse(((Warehouse) worker.get(0).getPrimaryLocation()));
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }
}
