package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.Credit;
import model.Debit;

import java.sql.Date;
import java.sql.Time;

public class CashBook {

    public Debit debit(double amount, String description) {
        Debit debit = new Debit();
        debit.setDate(new Date(System.currentTimeMillis()));
        debit.setAmount(amount);
        debit.setDescription(description);
        debit.setId(GlobalValues.generateId());
        debit.setWorker(GlobalValues.getWorker());
        debit.setTime(new Time(System.currentTimeMillis()));
        DBManager.save(Debit.class, debit);
        DBManager.begin();
        GlobalValues.getWorker().getDebits().add(debit);
        DBManager.commit();
        return debit;
    }

    public Credit credit(double amount, String description){
        Credit credit = new Credit();
        credit.setId(GlobalValues.generateId());
        credit.setDate(new Date(System.currentTimeMillis()));
        credit.setTime(new Time(System.currentTimeMillis()));
        credit.setWorker(GlobalValues.getWorker());
        credit.setDescription(description);
        credit.setAmount(amount);
        DBManager.save(Credit.class, credit);
        DBManager.begin();
        GlobalValues.getWorker().getCredits().add(credit);
        DBManager.commit();
        return credit;
    }
}
