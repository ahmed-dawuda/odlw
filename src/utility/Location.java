package utility;


import mStuff.DBManager;
import model.*;
import model.Inventory;
import model.Product;

import java.util.List;

public class Location {
    public model.Inventory getInventoryByProductAndWarehouse(Product product, Warehouse warehouse){
        Inventory inventory = null;
        List<Inventory> inventories = DBManager.query(Inventory.class, "Select i from Inventory i where i.product = ?1 and i.location = ?2", product, warehouse);
        if (inventories.size() > 0) inventory = inventories.get(0);
        return inventory;
    }
}
