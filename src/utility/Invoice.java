package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Location;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class Invoice {

    private CashBook cashBook = new CashBook();

    public List<model.Invoice> all(){
        return DBManager.query(model.Invoice.class, "SELECT i FROM Invoice i WHERE i.location = ?1", GlobalValues.getWarehouse());
    }

    public List<model.Invoice> getCashSales(){
        return DBManager.query(model.Invoice.class, "select i from Invoice i where i.type = ?1", "cash");
    }

    public model.Invoice create(Location location, String customername, double discount, double payable, double total, String type){
        model.Invoice invoice = new model.Invoice();
        invoice.setId(GlobalValues.generateId());
        invoice.setDate(new Date(System.currentTimeMillis()));
        invoice.setTime(new Time(System.currentTimeMillis()));
        invoice.setCustomer(customername.trim());
        invoice.setDiscount(discount);
        invoice.setPayable(payable);
        invoice.setTotal(total);
        invoice.setWorker(GlobalValues.getWorker());
        invoice.setLocation(location);
        invoice.setType(type);
        DBManager.save(model.Invoice.class, invoice);

        //crediting cashbook after making sale
        Credit credit = cashBook.credit(payable, "cash sales to " + customername);
//        DBManager.begin();
//        GlobalValues.getWorker().getCredits().add(credit);
//        DBManager.commit();

        return invoice;
    }

    public List<model.Invoice> filterByDates(Date start, Date end){
        return DBManager.query(model.Invoice.class, "select i from Invoice i where i.date >= ?1 and i.date <= ?2 and i.location = ?3",
                start, end, GlobalValues.getWarehouse());
    }

    public CreditSaleInvoice createCreditSaleInvoice(Location location, String customername, double discount, double payable, double total, Customer customer, double initial){
        CreditSaleInvoice creditSaleInvoice = new CreditSaleInvoice();
        creditSaleInvoice.setId(GlobalValues.generateId());
        creditSaleInvoice.setLocation(location);
        creditSaleInvoice.setWorker(GlobalValues.getWorker());
        creditSaleInvoice.setType("credit");
        creditSaleInvoice.setPayable(payable);
        creditSaleInvoice.setAmountPaid(initial);
        creditSaleInvoice.setBalance(payable - initial);
        creditSaleInvoice.setDebtor(customer);
        creditSaleInvoice.setCustomer(customername);
        creditSaleInvoice.setDiscount(discount);
        creditSaleInvoice.setDate(new Date(System.currentTimeMillis()));
        creditSaleInvoice.setTime(new Time(System.currentTimeMillis()));
        creditSaleInvoice.setTotal(total);
        DBManager.save(CreditSaleInvoice.class, creditSaleInvoice);
        if (initial > 0) {
            cashBook.credit(initial, "credit sale to "+ customername);
        }
        return creditSaleInvoice;
    }

    public List<CreditSaleInvoice> creditSaleInvoices(){
        return DBManager.listAll(CreditSaleInvoice.class);
    }
}
