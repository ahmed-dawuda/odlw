package wrappers;


import mStuff.GlobalValues;
import model.*;
import model.Location;
import model.Product;
import model.Transfer;
import utility.*;
import utility.Inventory;

import java.util.List;

public class GoodWrapper {
    private Product product;
    private double quantityApproved;
    private Warehouse supplyFrom;
    private double instock = 0;
    private Number id;

    private utility.Inventory inventoryUtil = new Inventory();

    public GoodWrapper(){}

    public GoodWrapper(Good good){
        this.product = good.getProduct();
        this.quantityApproved = good.getQuantity();
        List<Location> locations = GlobalValues.getWorker().getLocations();
        model.Inventory inventoryByProduct1 = inventoryUtil.getInventoryByProduct(product);

        if (inventoryByProduct1.getQuantity() < good.getQuantity()) {

            for (Location location : locations) {
                model.Inventory inventoryByProductAndLocation = inventoryUtil.getInventoryByProductAndLocation(this.product, (Warehouse) location);
                if (inventoryByProductAndLocation != null) {
                    if (inventoryByProductAndLocation.getQuantity() >= good.getQuantity()) {
                        this.supplyFrom = (Warehouse) location;
                        this.instock = inventoryByProductAndLocation.getQuantity();
                        break;
                    }
                }
            }
        } else {
            this.instock = inventoryByProduct1.getQuantity();
            this.supplyFrom = GlobalValues.getWarehouse();
        }

        this.id = good.getId();
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantityApproved() {
        return quantityApproved;
    }

    public void setQuantityApproved(double quantityApproved) {
        this.quantityApproved = quantityApproved;
    }

    public Warehouse getSupplyFrom() {
        return supplyFrom;
    }

    public void setSupplyFrom(Warehouse supplyFrom) {
        this.supplyFrom = supplyFrom;
    }

    public double getInstock() {
        return instock;
    }

    public void setInstock(double instock) {
        this.instock = instock;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public Good toGood(Transfer transfer){
        Good good = new Good();
        good.setId(GlobalValues.generateId());
        good.setInstock(this.instock);
        good.setQuantity(this.quantityApproved);
        good.setProduct(this.product);
        good.setTransfer(transfer);
        return good;
    }
}
