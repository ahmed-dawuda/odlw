package wrappers;


import java.sql.Date;
import java.sql.Time;

public class Payment {
    private Date date;
    private Time time;
    private String bank;
    private Double amount;
    private String branch;
    private String account;
}
